import org.bson.codecs.pojo.annotations.BsonDiscriminator;

@BsonDiscriminator
public class Shop {

    private String name;
    private Goods goods;

    public Shop(String name, Goods goods) {
        this.name = name;
        this.goods = goods;
    }

    public Shop(String name) {
        this.name = name;
    }

    public Shop() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }
}
