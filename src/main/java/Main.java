import com.mongodb.MongoClient;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.ClassModel;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class Main {

    private static final MongoCollection<Goods> goods = setUpMongoDBProperties().getCollection("goods", Goods.class);
    private static final MongoCollection<Shop> shop = setUpMongoDBProperties().getCollection("shop", Shop.class);
    private static final ArrayList<Goods> goodsList = goods.find().into(new ArrayList<>());

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите команду:");
        while (true) {
            String command = scanner.nextLine();
            if (command.equals("0")) {
                break;
            }
            String[] commandWords = command.split(" ");
            switch (commandWords[0]) {
                case "ADD_SHOP" -> addShop(commandWords[1]);
                case "ADD_GOODS" -> addGoods(commandWords[1], commandWords[2]);
                case "INSERT_GOODS" -> addGoodsInShop(commandWords[1], commandWords[2]);
                case "GOODS_STATISTICS" -> goodsInfo();
            }
        }
    }


    private static MongoDatabase setUpMongoDBProperties() {
        CodecRegistry codecRegistry = CodecRegistries.fromRegistries(
                MongoClientSettings.getDefaultCodecRegistry(),
                CodecRegistries.fromProviders(PojoCodecProvider.builder()
                        .register(
                                ClassModel.builder(Shop.class).enableDiscriminator(true).build(),
                                ClassModel.builder(Goods.class).enableDiscriminator(true).build()
                        ).automatic(true)
                        .build()
                )
        );
        return new MongoClient("127.0.0.1", 27017)
                .getDatabase("database").withCodecRegistry(codecRegistry);
    }

    private static void addShop(String command) {
        shop.insertOne(new Shop(command.trim()));
    }

    private static void addGoods(String command, String amount) {
        goods.insertOne(new Goods(command.trim(), Integer.parseInt(amount.trim())));
    }

    private static void addGoodsInShop(String goodsName, String shopsName) {
        shop.insertOne(new Shop(goodsName, new Goods(shopsName)));
    }

    private static void goodsInfo() {
        System.out.println("Общее количество наименований товаров: " + goodsAmount());
        System.out.println("Средняя цена товаров: " + averageGoodsPrice());
        System.out.println("Самый дорогой товар: " + theMostExpensiveGoods());
        System.out.println("Самый дешевый товар: " + theCheapestGoods());
        System.out.println("Количество товаров дешевле 100 рублей: " + goodsCheaperThanAHundredRubles());
    }

    private static int goodsAmount() {
        return goodsList.size();
    }

    private static double averageGoodsPrice() {
        int totalPrice = 0;
        double average = 0;
        for (Goods g : goodsList) {
            totalPrice += g.getPrice();
            average = (double) totalPrice / goodsList.size();
        }
        return average;
    }

    private static Goods theMostExpensiveGoods() {

        return goodsList.stream().max(Comparator.comparing(Goods::getPrice)).get();
    }

    private static Goods theCheapestGoods() {
        return goodsList.stream().min(Comparator.comparing(Goods::getPrice)).get();
    }

    private static long goodsCheaperThanAHundredRubles() {
        return goodsList.stream().filter(g -> g.getPrice() < 100).count();
    }
}
